from django.contrib import admin
from .models import Alojamiento,Habitacion,Servicio_extra,CategoriaAlojamiento,GaleriaHab,Marcador
from pagedown.widgets import AdminPagedownWidget
from django.db import models
# Register your models here.
class ImagesInline(admin.TabularInline):
  model = GaleriaHab

class HabitacionAdmin(admin.ModelAdmin):
  inlines = [
    ImagesInline,
  ]
 
class AlojamientoAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('nombre',)}
	formfield_overrides = {
		models.TextField:{'widget':AdminPagedownWidget},
	}

class CategoriaAlojamientoAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('nombre',)}
	formfield_overrides = {
		models.TextField:{'widget':AdminPagedownWidget},
	}

admin.site.register(Alojamiento,AlojamientoAdmin)
admin.site.register(CategoriaAlojamiento,CategoriaAlojamientoAdmin)
admin.site.register(Habitacion,HabitacionAdmin)
admin.site.register(Servicio_extra)
admin.site.register(Marcador)