from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse

# Create your models here.


class CategoriaAlojamiento(models.Model):
    nombre = models.CharField(max_length=250, db_index=True)
    slug = models.SlugField(max_length=250, db_index=True, unique=True)

    class Meta():
        ordering = ('nombre',)
        verbose_name = 'categoria_alojamiento'
        verbose_name_plural = 'categorias_alojamiento'

    def __str__(self):
        return self.nombre

    def get_absolute_url(self):
        return reverse('alojamientos:alojamiento_list_by_categoria', args=[self.slug])


class Alojamiento(models.Model):
    categoria = models.ForeignKey(CategoriaAlojamiento, related_name='categoria')
    nombre = models.CharField(max_length=100)
    slug = models.SlugField(max_length=100, unique=True)
    descripcion = models.TextField(max_length=500)
    logo = models.ImageField(upload_to='logo-hotel',default='img/teo2.png', null=True, blank=True)
    foto_portada = models.ImageField(upload_to='portadas', default='img/teo2.png', null=True, blank=True)
    foto_portada2 = models.ImageField(upload_to='portadas', default='img/teo2.png', null=True, blank=True)
    foto_portada3 = models.ImageField(upload_to='portadas', default='img/teo2.png', null=True, blank=True)
    direccion = models.CharField(max_length=200)
    lat = models.FloatField(null=True, blank=True, default=None)
    lng = models.FloatField(null=True, blank=True, default=None)
    tel_1 = models.CharField(max_length=30, blank=True, null=True)
    tel_2 = models.CharField(max_length=30, blank=True, null=True)
    correo = models.EmailField(max_length=100, blank=True,null=True,default='contacto@teotihuacanmagico.com.mx')
    facebook = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
    twitter =models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
    instagram = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
    youtube = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
    sitio_web = models.URLField(max_length=500, blank=True, null=True)

    bar = models.BooleanField(default=False)
    wifi = models.BooleanField(default=False)
    estacionamiento = models.BooleanField(default=False)
    servicio_habitacion = models.BooleanField(default=False)
    desayuno = models.BooleanField(default=False)
    ninos = models.BooleanField(default=True)
    restaurant = models.BooleanField(default=False)
    spa = models.BooleanField(default=False)
    alberca = models.BooleanField(default=False)

    available = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre

    def get_absolute_url(self):
        return reverse('alojamientos:alojamiento_detail', args=[self.slug])

class Marcador(models.Model):
    alojamiento = models.ForeignKey(Alojamiento, related_name='alojamiento')
    nombre = models.CharField(max_length=250, blank=True, null=True)
    latitud = models.FloatField(null=True,blank=True, default=None)
    longitud = models.FloatField(null=True,blank=True,default=None)

    def __str__(self):
        return self.nombre

class Habitacion(models.Model):
    tipo = models.CharField(max_length=100)
    precio = models.CharField(max_length=100)
    alojamiento = models.ForeignKey(Alojamiento, on_delete=models.CASCADE)

    def __str__(self):
        return (self.alojamiento.nombre)

    def get_galeria(self):
        galeria = GaleriaHab.objects.filter(habitacion=self)
        return galeria

class GaleriaHab(models.Model):
    image = models.ImageField(upload_to="galeria-alojamiento",default='img/teo2.png',null=True,blank=True)
    habitacion = models.ForeignKey(Habitacion,on_delete=models.CASCADE)

    def __str__(self):
        return (self.habitacion.tipo)

class Servicio_extra(models.Model):
    tipo = models.CharField(max_length=50)
    alojamiento_servicio = models.ForeignKey(Alojamiento, on_delete=models.CASCADE)

    def __str__(self):
        return (self.alojamiento_servicio.nombre)
