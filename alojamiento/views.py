from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from .models import Alojamiento, Servicio_extra, Habitacion, CategoriaAlojamiento,GaleriaHab,Marcador


class Alojamiento_List(View):

    def get(self, request, categoria_slug=None):
        template_name = "alojamiento_list.html"
        categoria = None
        categorias = CategoriaAlojamiento.objects.all()
        alojamientos = Alojamiento.objects.filter(available=True)
        if categoria_slug:
            categoria = get_object_or_404(
                CategoriaAlojamiento, slug=categoria_slug)
            alojamientos = alojamientos.filter(categoria=categoria)
        context = {
            'categoria': categoria,
            'categorias': categorias,
            'alojamientos': alojamientos,
        }
        print(alojamientos,categoria)
        return render(request, template_name, context)


class Alojamiento_Detail(View):

    def get(self, request, slug):
        template_name = "alojamiento_detail.html"
        alojamiento = get_object_or_404(Alojamiento, slug=slug)
        servicios_extra = Servicio_extra.objects.all().filter(alojamiento_servicio=alojamiento)
        habitaciones = Habitacion.objects.all().filter(alojamiento=alojamiento)
        marcadores = Marcador.objects.all().filter(alojamiento = alojamiento)
        context = {
            'alojamiento': alojamiento,
            'servicios_extra': servicios_extra,
            'habitaciones': habitaciones,
            'marcadores':marcadores,
        }
        return render(request, template_name, context)
