from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^alojamiento-list/$', views.Alojamiento_List.as_view(),name="alojamiento_list"),
    url(r'^alojamiento-list/(?P<categoria_slug>[-\w]+)/$',views.Alojamiento_List.as_view(), name="alojamiento_list_by_categoria"),
    url(r'^alojamiento-detail/(?P<slug>[-\w]+)/$',views.Alojamiento_Detail.as_view(), name="alojamiento_detail"),
    # url(r'^alojamiento-detail/(?P<id>\d+)/$',views.Alojamiento_Detail.as_view(), name="alojamiento_detail"),
]
