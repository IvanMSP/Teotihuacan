from django.conf.urls import url
from . import views

urlpatterns =[
	url(r'^evento-list/$',views.Evento_List.as_view(), name="evento_list"),
	url(r'^evento-list/(?P<categoria_slug>[-\w]+)/$',views.Evento_List.as_view(),name="evento_list_by_categoria"),
	url(r'^evento-detail/(?P<slug>[-\w]+)/$',views.Evento_Detail.as_view(), name="evento_detail"),
	# url(r'^evento-detail/(?P<id>\d+)/(?P<slug>[-\w]+)/$',views.Evento_Detail.as_view(), name="evento_detail"),
]	