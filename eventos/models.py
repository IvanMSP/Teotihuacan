from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse
from datetime import datetime
# Create your models here.

class CategoriaEvento(models.Model):
	nombre = models.CharField(max_length=300,db_index=True)
	slug = models.SlugField(max_length=300,db_index=True, unique=True)

	class Meta():
		ordering = ('nombre',)
		verbose_name = 'categoria_evento'
		verbose_name_plural = "categorias_eventos"
	
	def __str__(self):
		return self.nombre

	def get_absolute_url(self):
		return reverse('eventos:evento_list_by_categoria', args=[self.slug])

class Evento(models.Model):
	categoria = models.ForeignKey(CategoriaEvento, related_name='categoria', blank=True , null=True)
	nombre = models.CharField(max_length=100,db_index=True)
	slug = models.SlugField(max_length=100,db_index=True)
	logo = models.ImageField(upload_to='logo-eventos',default='img/teo2.png',blank=True,null=True)
	portada = models.ImageField(upload_to='portada-eventos',default='img/teo2.png', blank= True, null=True)
	portada_2 = models.ImageField(upload_to='portada-eventos', default='img/teo2.png',blank=True,null=True)
	portada_3 = models.ImageField(upload_to='portada-eventos',default='img/teo2.png',blank=True,null=True)
	descripcion = models.TextField()
	fecha_inicial = models.DateTimeField( blank=True, null=True)
	fecha_final = models.DateTimeField(blank=True,null=True)
	fecha_unica = models.DateTimeField( blank=True,null=True)
	direccion = models.CharField(max_length=300)
	lat = models.FloatField(null=True,blank=True, default=None)
	lng = models.FloatField(null=True,blank=True, default=None)	
	tel_1 = models.CharField(max_length=20,blank=True,null=True, default='No Disponible')
	tel_2 = models.CharField(max_length=20,blank=True,null=True,default='No Disponible')
	id_video = models.CharField(max_length=250, null=True, blank=True)
	is_gratis = models.BooleanField(default=True)
	correo = models.EmailField(max_length=100, blank=True,null=True,default='teotihuacanmagico@gmail.com')
	facebook = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
	twitter =models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
	instagram = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
	youtube = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
	sitio_web = models.URLField(max_length=500,blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
	url_boletos = models.URLField(blank=True,null=True,default='www.teotihuacanmagico.com.mx')
	available = models.BooleanField(default=True)
	programa = models.TextField(blank=True,null=True)


	estacionamiento = models.BooleanField(default=False)
	banos = models.BooleanField(default=False)

	def __str__(self):
		return self.nombre

	def get_absolute_url(self):
		return reverse('eventos:evento_detail', args=[self.slug])

class Marcador(models.Model):
	evento = models.ForeignKey(Evento, related_name='evento')
	nombre = models.CharField(max_length=250, blank=True,null=True)
	latitud = models.FloatField(null=True, blank=True,default=None)
	longitud = models.FloatField(null=True, blank=True,default=None)

	def __str__(self):
		return self.nombre

class Precio(models.Model):
	tipo = models.CharField(max_length=140,blank=True, null=True)
	costo = models.DecimalField(max_digits=10,decimal_places=2)
	descripcion = models.TextField(max_length=500)
	evento= models.ForeignKey(Evento, related_name='precios', on_delete=models.CASCADE)

	def __str__(self):
		return (self.evento.nombre)

class Organizador(models.Model):
	nombre_org = models.CharField(max_length=250,blank=True,null=True)
	slug = models.SlugField(max_length=250)
	descripcion_org = models.TextField()
	evento_org = models.ForeignKey(Evento,related_name='organizadores', on_delete=models.CASCADE)

	def __str__(self):
		return (self.evento_org.nombre)