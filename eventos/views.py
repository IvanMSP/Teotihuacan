from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from .models import Evento,Precio,Organizador,CategoriaEvento,Marcador



class Evento_List(View):
	def get(self,request,categoria_slug = None):
		template_name = "evento_list.html"
		categoria = None
		categorias = CategoriaEvento.objects.all()
		eventos = Evento.objects.filter(available=True)
		if categoria_slug:
			categoria = get_object_or_404(CategoriaEvento, slug = categoria_slug)
			eventos = eventos.filter(categoria = categoria)
		context = {
			'categoria':categoria,
			'categorias':categorias,
			'eventos':eventos,
		}
		print(eventos,categoria)
		return render(request,template_name,context)
		
class Evento_Detail(View):

	def get(self,request,slug):
		template_name = "evento_detail.html"
		evento = get_object_or_404(Evento,slug=slug)
		precios = Precio.objects.all().filter(evento=evento)
		organizadores = Organizador.objects.all().filter(evento_org=evento)
		marcadores = Marcador.objects.all().filter(evento = evento)
		context = {
			'evento':evento,
			'precios':precios,
			'organizadores':organizadores,
			'marcadores':marcadores,
		}
		return render(request,template_name,context)
# Create your views here.
