from django.contrib import admin
from .models import Evento,Precio,Organizador,CategoriaEvento,Marcador
from pagedown.widgets import AdminPagedownWidget
from django.db import models
# Register your models here.

class EventoAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('nombre',)}
	formfield_overrides={
		models.TextField:{'widget':AdminPagedownWidget},
	}

class PrecioAdmin(admin.ModelAdmin):
	formfield_overrides={
		models.TextField:{'widget':AdminPagedownWidget},
	}

class OrganizadorAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('nombre_org',)}

class CategoriaAdmin(admin.ModelAdmin):
	prepopulated_fields= {"slug":('nombre',)}

admin.site.register(Evento,EventoAdmin)
admin.site.register(CategoriaEvento,CategoriaAdmin)
admin.site.register(Precio,PrecioAdmin)
admin.site.register(Organizador,OrganizadorAdmin)
admin.site.register(Marcador)
