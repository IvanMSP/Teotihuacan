from django.shortcuts import render
from django.views.generic import View
from actividades.models import Actividad
from eventos.models import Evento
from alojamiento.models import Alojamiento
from restaurantes.models import Restaurant
from .forms import ContactForm
from django.core.mail import send_mail 

class Home(View):
    def get(self,request):
        template_name = "home.html"
        form = ContactForm()
        
        context = {
            'form':form,
            
        }
        return render(request, template_name, context)


    def post(self, request):
        template_name = 'home.html'
        form = ContactForm(request.POST)
        if form.is_valid():
            template = 'thanks.html'
            data = form.cleaned_data
            message = 'De {} \n\n Mensaje: {}'.format(data['email'], data['content'])
            send_mail(
                data['subject'],
                message,
                # data['email'],
                'admin@mail.com',
                ['axio.valores@gmail.com'],
                fail_silently = True,
            )
            return render(request, template)
        else:
            form = ContactForm()
        context = {
            'form':form,
        }
        return render(request, template_name, context)


# Create your views here.
class TiendaView(View):
    def get(self,request):
        template_name ="tienda.html"
        return render(request,template_name)

class PrestadoresView(View):
    def get(self,request):
        template_name = 'prestadores.html'
        return render(request,template_name)

class SiteWeb(View):
    def get(self,request):
        template_name = 'site_web.html'
        return render(request,template_name)
        