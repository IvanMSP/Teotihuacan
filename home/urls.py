from django.conf.urls import url
from . import views

urlpatterns =[
	url(r'^$',views.Home.as_view(),name="home"),
	url(r'^tienda/$',views.TiendaView.as_view(),name="tienda"),
	url(r'^prestadores-servicio/$',views.PrestadoresView.as_view(), name = "prestadores"),
	url(r'^sitios-web/$',views.SiteWeb.as_view(), name = "siteweb"),
]