from django import template
from django.utils.safestring import mark_safe
import markdown

register = template.Library()

#from ..models import Post

@register.filter(name='markdown')
def markdown_format(text):
    return mark_safe(markdown.markdown(text))
