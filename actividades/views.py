from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from .models import Actividad, Categoria, Precio, Marcador


class Activity_List(View):

    def get(self, request, categoria_slug=None):
        template_name = "actividad_list.html"
        categoria = None
        categorias = Categoria.objects.all()
        actividades = Actividad.objects.filter(available=True)
        if categoria_slug:
            categoria = get_object_or_404(Categoria, slug=categoria_slug)
            actividades = actividades.filter(categoria=categoria)
        context = {
            'categoria': categoria,
            'categorias': categorias,
            'actividades': actividades,
        }
        return render(request, template_name, context)


class Activity_Detail(View):

    def get(self, request, slug):
        template_name = "actividad_detail.html"
        actividad = get_object_or_404(Actividad, slug=slug)
        precios = Precio.objects.all().filter(actividad=actividad)
        marcadores = Marcador.objects.all().filter(actividad=actividad)
        # horarios = Horario.objects.all().filter(actividadh=actividad)
        context = {
            'actividad': actividad,
            'precios': precios,
            'marcadores':marcadores,
            # 'horarios': horarios,
        }
        return render(request, template_name, context)
# Create your views here.
