from django.contrib import admin
from .models import Actividad,Precio,Categoria,Marcador
from pagedown.widgets import AdminPagedownWidget
from django.db import models
# Register your models here.
class ActividadAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('nombre',)}
	formfield_overrides = {
		models.TextField:{'widget':AdminPagedownWidget},
	}

class PrecioAdmin(admin.ModelAdmin):
	formfield_overrides={
		models.TextField:{'widget':AdminPagedownWidget},
	}

class CategoriaAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('nombre',)}

admin.site.register(Actividad,ActividadAdmin)
admin.site.register(Precio,PrecioAdmin)
admin.site.register(Categoria,CategoriaAdmin)
admin.site.register(Marcador)
# admin.site.register(Horario)