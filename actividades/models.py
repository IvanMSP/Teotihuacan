from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse
from datetime import datetime
import os

def get_image_path(instance, filename):
    return os.path.join('photos', str(instance.id), filename)
# Create your models here.
class Categoria(models.Model):
	nombre = models.CharField(max_length=250, db_index=True)
	slug = models.CharField(max_length=250, db_index=True, unique=True)

	class Meta():
		ordering = ('nombre',)
		verbose_name='categoria'
		verbose_name_plural='categorias'

	def __str__(self):
		return self.nombre

	def get_absolute_url(self):
		return reverse ('actividades:actividad_list_by_categoria', args=[self.slug])

class Actividad(models.Model):
	categoria = models.ForeignKey(Categoria,related_name='actividades')
	nombre = models.CharField(max_length=200,db_index=True)
	slug = models.SlugField(max_length=200,db_index=True)
	logo = models.ImageField(upload_to='logo-actividades', blank=True, null=True)
	portada1 = models.ImageField(upload_to=get_image_path, blank=True,null=True)
	portada2 = models.ImageField(upload_to=get_image_path, blank=True,null=True)
	portada3 = models.ImageField(upload_to=get_image_path, blank=True,null=True)
	fecha_inicial = models.DateTimeField( blank=True, null=True)
	fecha_final = models.DateTimeField(blank=True,null=True)
	fecha_unica = models.DateTimeField( blank=True,null=True)
	horario = models.CharField(max_length=40,blank=True,null=True)
	descripcion = models.TextField()
	ubicacion = models.CharField(max_length=200)
	sugerencia = models.TextField(blank=True,null=True)
	telefono = models.CharField(max_length=150,blank=True,null=True)
	telefono2 = models.CharField(max_length=150,blank=True,null=True)
	correo = models.EmailField(max_length=100, blank=True,null=True,default='contacto@teotihuacanmagico.com.mx')
	facebook = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
	twitter =models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
	instagram = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
	youtube = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
	sitio_web = models.URLField(max_length=500,blank=True,null=True,default='http://www.teotihuacanmagico.com.mx')
	available= models.BooleanField(default=True)
	lat = models.FloatField(null=True, blank=True, default=None)
	lng = models.FloatField(null=True, blank=True, default=None)

	def __str__(self):
		return (self.nombre)

	def get_absolute_url(self):
		return reverse('actividades:actividad_detail',args=[self.slug])

class Precio(models.Model):
	tipo = models.CharField(max_length=140,blank=True, null=True)
	costo = models.DecimalField(max_digits=10,decimal_places=2)
	incluye = models.TextField(null=True,blank=True)
	no_incluye = models.TextField(null=True,blank=True)
	actividad = models.ForeignKey(Actividad,related_name='actividades', on_delete=models.CASCADE)

	def __str__(self):
		return (self.actividad.nombre)

class Marcador(models.Model):
    actividad = models.ForeignKey(Actividad, related_name='actividad')
    nombre = models.CharField(max_length=250, blank=True, null=True)
    latitud = models.FloatField(null=True,blank=True, default=None)
    longitud = models.FloatField(null=True,blank=True,default=None)

    def __str__(self):
        return self.nombre
# class Horario(models.Model):
# 	actividadh = models.ForeignKey(Actividad,related_name='actividadh',on_delete=models.CASCADE)
# 	nombre = models.CharField(max_length=250,blank=True,null=True)
# 	horas = models.CharField(max_length=50,blank=True,null=True)

# 	def __str__(self):
# 		return (self.actividadh.nombre)