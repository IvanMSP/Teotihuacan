from django.conf.urls import url
from . import views

urlpatterns =[
	url(r'^actividad-list/$',views.Activity_List.as_view(), name="actividad_list"),
	url(r'^actividad-list/(?P<categoria_slug>[-\w]+)/$',views.Activity_List.as_view(),name="actividad_list_by_categoria"),
	url(r'^actividad-detail/(?P<slug>[-\w]+)/$',views.Activity_Detail.as_view(), name="actividad_detail"),
	# url(r'^actividad-detail/(?P<id>\d+)/(?P<slug>[-\w]+)/$',views.Activity_Detail.as_view(), name="actividad_detail"),
]