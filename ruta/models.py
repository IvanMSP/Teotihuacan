from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse
# Create your models here.


class Lugar(models.Model):
	nombre = models.CharField(max_length = 250)
	slug = models.SlugField(max_length = 250 , unique=True)
	direccion = models.CharField(max_length = 500)
	logo = models.ImageField(upload_to = 'logo-ruta',default = 'img/teo2.png', blank=True, null=True)
	telefono = models.CharField(max_length=50)
	telefono2 = models.CharField(max_length=50)
	correo = models.EmailField(max_length=100,default='contacto@teotihuacanmagico.com.mx')
	image_uno = models.ImageField(upload_to='rutas',default='img/teo2.png')
	image_dos = models.ImageField(upload_to='rutas',default='img/teo2.png')
	image_tres = models.ImageField(upload_to='rutas',default='img/teo2.png')
	lat = models.FloatField(null=True, blank=True, default=None)
	lng = models.FloatField(null=True, blank=True, default=None)
	available = models.BooleanField(default=True)

	def __str__(self):
		return self.nombre

	def get_absolute_url(self):
	 	return reverse('ruta:lugar_detail', args=[self.slug])


class Salida(models.Model):
	lugar_s = models.ForeignKey(Lugar, related_name='lugar_s')
	hora_salida = models.CharField(max_length=25)
	origen = models.CharField(max_length=250)
	destino = models.CharField(max_length=250)
	precio = models.CharField(max_length=250)

	def __str__(self):
		return self.lugar_s.nombre


class Marcador(models.Model):
	lugar_m = models.ForeignKey(Lugar, related_name='lugar_m')
	latitud = models.FloatField(null=True, blank=True, default=None)
	longitud = models.FloatField(null=True, blank=True, default=None)

	def __str__(self):
		return self.lugar_m.nombre