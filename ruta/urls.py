from django.conf.urls import url 
from . import views

urlpatterns = [
	url(r'^lugar-list/$',views.Lugar_List.as_view(), name="lugar_list"),
	url(r'^lugar-detail/(?P<slug>[-\w]+)/$',views.Lugar_Detail.as_view(), name="lugar_detail"),
]