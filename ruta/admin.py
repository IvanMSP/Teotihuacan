from django.contrib import admin
from .models import Lugar,Salida,Marcador
from pagedown.widgets import AdminPagedownWidget
from django.db import models

# Register your models here.
class LugarAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('nombre',)}
	formfield_overrides = {
		models.TextField:{'widget':AdminPagedownWidget},
	}

admin.site.register(Lugar,LugarAdmin)
admin.site.register(Salida)
admin.site.register(Marcador)
