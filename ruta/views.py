from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from .models import Lugar,Salida,Marcador

class Lugar_List(View):
	def get(self,request):
		template_name = "lugar_list.html"
		marcadores = Marcador.objects.all()
		lugares = Lugar.objects.all().filter(available= True)
		context = {
			'lugares':lugares,
			'marcadores':marcadores,
		}
		#print(lugares,marcadores)
		return render(request,template_name,context)

class Lugar_Detail(View):
	def get(self,request,slug):
		template_name = "lugar_detail.html"
		lugar = get_object_or_404(Lugar,slug = slug)
		marcadores = Marcador.objects.all().filter(lugar_m = lugar)
		salidas = Salida.objects.all().filter(lugar_s = lugar)
		context ={
			'lugar':lugar,
			'marcadores':marcadores,
			'salidas': salidas,
		}
		print(lugar,marcadores)
		return render(request,template_name,context)
# Create your views here.
