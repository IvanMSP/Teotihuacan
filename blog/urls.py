from django.conf.urls import url
from . import views

urlpatterns = [
	url(r'^$', views.PostList.as_view(), name='post_list'),
	url(r'^tag/(?P<tag_slug>[-\w]+)/$', views.PostList.as_view(), name="post_list_by_tag"),
	url(r'^list/$', views.PostListView.as_view(), name='post_list_view'),
	url(r'^(?P<post>[-\w]+)/$', views.PostDetailView.as_view(), name="post_detail"),
	url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<post>[-\w]+)/$', views.PostDetail.as_view(), name="post_detail"),
	url(r'^(?P<year>\d{4})/(?P<month>\d{2})/(?P<day>\d{2})/(?P<post>[-\w]+)/(?P<ready>\d+)/$', views.PostDetail.as_view(), name="post_detail_ready"),
	url(r'^(?P<post_id>\d+)/share/$', views.SentEmailView.as_view(), name="post_share"),
	url(r'^(?P<post_id>\d+)/(?P<conf>\d+)/share/$', views.SentEmailView.as_view(), name="post_share_conf"),
]