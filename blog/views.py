from django.shortcuts import render, get_object_or_404, redirect
from django.core.urlresolvers import reverse
from django.views.generic import View, ListView
from .models import Post, Comment
from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from .forms import EmailPostForm, CommentForm

#Para el envio de Email
from django.core.mail import send_mail

#Taggit
from taggit.models import Tag

from django.db.models import Count

# Create your views here.

# Enviar Email 
class SentEmailView(View):
	def get(self, request, post_id, conf=None):
		sent = False
		template_name = 'blog/share.html'
		post = get_object_or_404(Post, id=post_id, status='published')
		form = EmailPostForm()
		if conf:
			if conf == '1':
				sent = True
		context = {
			'form':form,
			'post':post,
			'sent':sent,
		}
		return render(request, template_name, context)

	def post(self, request, post_id):
		template_name = 'blog/share.html'	
		post = get_object_or_404(Post, id=post_id, status='published')
		sent = False
		form = EmailPostForm(request.POST)
		if form.is_valid():
			cd = form.cleaned_data
			##Obtener el url de nuestro post
			post_url = request.build_absolute_uri(post.get_absolute_url())
			subject = '{} ({}) te recomienda leer "{}"'.format(cd['nombre'], cd['correo'], post.title)
			message = 'Leer "{}" de {}\n\n comentarios de {}: {}'.format(post.title, post_url, cd['nombre'], cd['comentarios'])
			send_mail(subject, message, 'admin@my_blog.com', [cd['para']])
			sent = True
			conf = 1
			return redirect('blog:post_share_conf', post.id, conf)
			# ... enviando email
		else:
			form = EmailPostForm()
		context = {
			'post':post,
			'form':form,
			'sent:':sent,
		}
		return render(request, template_name, context)

class PostList(View):
	def get(self, request, tag_slug=None):
		template_name = "blog/post_list.html"
		# object_list = Post.published.all()
		object_list = Post.objects.order_by('title')
		#taggit
		tag = None
		if tag_slug:
			tag = get_object_or_404(Tag, slug=tag_slug)
			object_list = object_list.filter(tags__in=[tag])
		paginator = Paginator(object_list, 4) #6 post en cada pagina
		page = request.GET.get('page')
		try:
			posts = paginator.page(page)
		except PageNotAnInteger:
			posts = paginator.page(1)
		except EmptyPage:
			posts = paginator.page(paginator.num_pages)
		context = {
			'page':page,
			'posts':posts,
			'tag':tag,
		}
		return render(request, template_name, context)

class PostListView(ListView):
	queryset = Post.published.all()
	context_object_name = 'posts'
	paginate_by = 3
	template_name = 'blog/post_list_view.html'

class PostDetail(View):
	def get(self, request, year, month, day, post, ready = None):
		if ready:
			if ready == '1':
				ready = True
		template_name = "blog/post_detail.html"
		post = get_object_or_404(Post, slug=post,
			status = 'published',
			publish__year = year,
			publish__month = month,
			publish__day = day,)
		comments = post.comments.filter(active=True)
		comment_form = CommentForm()
		post_tags_ids = post.tags.values_list('id', flat=True)
		similar_posts = Post.published.filter(tags__in = post_tags_ids).exclude(id=post.id)
		similar_posts = similar_posts.annotate(same_tags=Count('tags')).order_by('-same_tags', '-publish')[:4]
		context = {
			'post':post,
			'comments':comments,
			'comment_form': comment_form,
			'ready':ready,
			'similar_posts':similar_posts,
		}
		return render(request, template_name, context)

	def post(self, request, year, month, day, post):
		ready = False
		post = get_object_or_404(Post, slug=post,
			status = 'published',
			publish__year = year,
			publish__month = month,
			publish__day = day,)
		comments = post.comments.filter(active=True)
		comment_form = CommentForm(data=request.POST)
		if comment_form.is_valid():
			new_comment = comment_form.save(commit=False)
			new_comment.post = post
			new_comment.save()
			ready = 1
			return redirect('blog:post_detail_ready', post.publish.year, post.publish.strftime('%m'), post.publish.strftime('%d'), post.slug, ready)
		else:
			comment_form = ()
		post_tags_ids = post.tags.values_list('id', flat=True)
		similar_posts = Post.published.filter(tags__in = post_tags_ids).exclude(id=post.id)
		similar_posts = similar_posts.annotate(same_tags=Count('tags')).order_by('-same_tags', '-publish')[:4]
		context = {
			'post':post,
			'comments':comments,
			'comment_form': comment_form,
			'ready':ready,
			'similar_posts':similar_posts
		}
		return render(request, template_name, context)

class PostDetailView(View):
	def get(self, request, post, ready = None):
		if ready:
			if ready == '1':
				ready = True
		template_name = "blog/post_detail.html"
		post = get_object_or_404(Post, slug=post,
			status = 'published',)
		comments = post.comments.filter(active=True)
		comment_form = CommentForm()
		post_tags_ids = post.tags.values_list('id', flat=True)
		similar_posts = Post.published.filter(tags__in = post_tags_ids).exclude(id=post.id)
		similar_posts = similar_posts.annotate(same_tags=Count('tags')).order_by('-same_tags', '-publish')[:4]
		context = {
			'post':post,
			'comments':comments,
			'comment_form': comment_form,
			'ready':ready,
			'similar_posts':similar_posts,
		}
		return render(request, template_name, context)