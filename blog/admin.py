from django.contrib import admin
from django.db import models
from .models import Post, Comment

#pagedown para body de Post
from pagedown.widgets import AdminPagedownWidget

# Register your models here.
class PostAdmin(admin.ModelAdmin):
	list_display = ('title', 'slug', 'author', 'publish', 'status',)
	list_filter = ('status', 'created', 'publish', 'author',)
	search_fields = ('title', 'body',)
	prepopulated_fields = {'slug':('title',),}
	raw_id_fields = ('author',)
	date_hierarchy = 'publish'
	ordering = ['status', 'publish']
	formfield_overrides ={
		models.TextField:{'widget':AdminPagedownWidget},
	}
admin.site.register(Post, PostAdmin)

class CommentAdmin(admin.ModelAdmin):
	list_display = ('name', 'email', 'post', 'created', 'active')
	list_filter = ('active', 'created', 'updated')
	search_fileds = ('name', 'email', 'body')

admin.site.register(Comment, CommentAdmin)