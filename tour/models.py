from django.db import models
from django.core.urlresolvers import reverse

class CategoriaTour(models.Model):
    nombre = models.CharField(max_length = 250,db_index = True)
    slug = models.SlugField(max_length = 250, db_index = True, unique = True)

    class Meta():
        ordering = ('nombre',)
        verbose_name = 'categoria_tour'
        verbose_name_plural = 'categorias_tour'

    def __str__(self):
        return self.nombre

    def get_absolute_url(self):
        return reverse('tours:tour_list_by_categoria', args = [self.slug])

# Create your models here.
class Tour(models.Model):
    categoria = models.ForeignKey(CategoriaTour, related_name='categoria')
    nombre = models.name = models.CharField(max_length=100, blank=True, null=True)
    slug = models.SlugField(max_length=100, unique = True)
    duracion = models.CharField(max_length=50)
    incluye = models.TextField()
    cupo = models.CharField(max_length=20)
    iterinario = models.TextField()
    descripcion = models.TextField()
    ubicacion = models.CharField(max_length=500)
    lat = models.FloatField(null=True, blank=True, default=None)
    lng = models.FloatField(null=True, blank=True, default=None)
    logo = models.ImageField(upload_to='logo-tour', default='img/teo2.png',blank=True,null=True)
    portada = models.ImageField(upload_to='portada-tour', default='img/teo2.png',blank=True,null=True)
    portada2 = models.ImageField(upload_to='portada-tour', default='img/teo2.png',blank=True,null=True)
    portada3 = models.ImageField(upload_to='portada-tour', default='img/teo2.png',blank=True,null=True)
    available = models.BooleanField(default=True)
    facebook = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
    twitter =models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
    instagram = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
    youtube = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
    sitio_web = models.URLField(max_length=500,blank=True,null=True,default='http://www.teotihuacanmagico.com.mx')
    correo = models.EmailField(max_length=100,blank=True,null=True, default='teotihuacanmagico@outlook.com')
    recomendacion = models.TextField(blank = True , null = True)
    tel = models.CharField(max_length=30, blank = True, null=True)
    telCel = models.CharField(max_length = 30 , blank = True, null=True)
    whatsapp = models.CharField(max_length = 30 , blank = True, null=True)


    def __str__(self):
        return self.nombre
    
    def get_absolute_url(self):
        return reverse('tours:tour_detail',args=[self.slug])


class Precio(models.Model):
    tipo = models.CharField(max_length=140,blank=True, null=True)
    costo = models.DecimalField(max_digits=10,decimal_places=2)
    tour = models.ForeignKey(Tour,related_name='tours',on_delete=models.CASCADE)

    def __str__(self):
        return (self.tour.nombre)

class Marcador(models.Model):
    tour = models.ForeignKey(Tour, related_name='tour')
    nombre = models.CharField(max_length = 250, blank=True,null=True)
    latitud = models.FloatField(null=True,blank=True,default=None)
    longitud = models.FloatField(null=True,blank=True,default=None)

    def __str__(self):
        return self.nombre