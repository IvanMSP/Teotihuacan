from django.shortcuts import render,get_object_or_404
from django.views.generic import View
from . models import Tour,CategoriaTour,Precio,Marcador


# Create your views here.
class Tour_List(View):
    def get(self,request,categoria_slug = None):
        template_name = "tour_list.html"
        categoria = None
        categorias = CategoriaTour.objects.all()
        tours = Tour.objects.filter(available = True)
        if categoria_slug:
            categoria = get_object_or_404(CategoriaTour, slug = categoria_slug)
            tours = tours.filter(categoria = categoria)
        context = {
            'categoria': categoria,
            'categorias': categorias,
            'tours':tours,
        }
        print(categoria, tours)
        return render(request,template_name,context)

class Tour_Detail(View):
    def get(self,request,slug):
        template_name = "tour_detail.html"
        tour = get_object_or_404(Tour, slug = slug)
        precios = Precio.objects.all().filter(tour = tour)
        marcadores = Marcador.objects.all().filter(tour = tour)
        context = {
            'precios':precios,
            'tour': tour,
            'marcadores':marcadores
        }
        return render(request,template_name,context)