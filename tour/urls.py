from django.conf.urls import url
from . import views

urlpatterns =[
	url(r'^tour-list/$',views.Tour_List.as_view(), name="tour_list"),
	url(r'^tour-list/(?P<categoria_slug>[-\w]+)/$',views.Tour_List.as_view(),name = "tour_list_by_categoria"),
	url(r'^tour-detail/(?P<slug>[-\w]+)/$',views.Tour_Detail.as_view(), name = "tour_detail"),
	# url(r'^tour-detail/(?P<id>\d+)/$',views.Tour_Detail.as_view(), name="tour_detail"),
]