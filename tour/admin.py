from django.contrib import admin
from .models import Tour,Precio,CategoriaTour,Marcador
from pagedown.widgets import AdminPagedownWidget
from django.db import models
# Register your models here.
class TourAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('nombre',)}
	formfield_overrides = {
		models.TextField:{'widget':AdminPagedownWidget},
	}

class CategoriaTourAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('nombre',)}
	formfield_overrides = {
		models.TextField:{'widget':AdminPagedownWidget},
	}

admin.site.register(Tour,TourAdmin)
admin.site.register(CategoriaTour,CategoriaTourAdmin)
admin.site.register(Precio)
admin.site.register(Marcador)