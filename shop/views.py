from django.shortcuts import render,get_object_or_404
from .models import Category,Product
from django.views.generic import View
# Create your views here.

class ProductList(View):
    def get(self,request,category_slug=None):
        category = None
        categories = Category.objects.all()
        products = Product.objects.filter(available=True)
        if category_slug:
            category = get_object_or_404(Category,slug=category_slug)
            products = products.filter(category=category)
        return render(request,'list_shop.html',{'category':category,'categories':categories,'products':products})

