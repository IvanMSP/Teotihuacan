from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^product-list/$',views.ProductList.as_view(),name="list-product"),
]