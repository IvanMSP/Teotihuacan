from django.contrib import admin
from .models import Servicio,CategoriaServicio,Marcador
from pagedown.widgets import AdminPagedownWidget
from django.db import models

class ServicioAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('nombre',)}
	formfield_overrides = {
		models.TextField:{'widget':AdminPagedownWidget},
	}

class CategoriaServicioAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('nombre',)}

# Register your models here.
admin.site.register(Servicio, ServicioAdmin)
admin.site.register(CategoriaServicio, CategoriaServicioAdmin)
admin.site.register(Marcador)