from django.conf.urls import url
from . import views

urlpatterns =[
	url(r'^servicios-list/$',views.Servicio_List.as_view(), name="servicio_list"),
	url(r'^servicios-list/(?P<categoria_slug>[-\w]+)/$',views.Servicio_List.as_view(), name="servicio_list_by_categoria"),
    url(r'^servicio-detail/(?P<slug>[-\w]+)/$',views.Servicio_Detail.as_view(), name="servicio_detail"),
	# url(r'^servicio-detail/(?P<id>\d+)/$',views.Servicio_Detail.as_view(), name="servicio_detail"),
]