from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse

class CategoriaServicio(models.Model):
    nombre = models.CharField(max_length=250, db_index=True)
    slug = models.SlugField(max_length = 250, db_index=True, unique=True)

    class Meta():
        ordering = ('nombre',)
        verbose_name='categoria_servicio'
        verbose_name_plural = 'categorias_servicio'

    def __str__(self):
        return self.nombre

    def get_absolute_url(self):
        return reverse ('servicios:servicio_list_by_categoria', args=[self.slug])


# Create your models here.
class Servicio(models.Model):
    categoria = models.ForeignKey(CategoriaServicio, related_name='categoria')
    nombre = models.CharField(max_length=100, blank=True, null=True)
    slug = models.SlugField(max_length=100)
    descripcion = models.TextField()
    logo = models.ImageField(upload_to='logo-servicio',default='img/teo2.png', blank=True,null=True)
    portada = models.ImageField(upload_to='portada-servicio',default='img/teo2.png', blank=True,null=True)
    ubicacion = models.CharField(max_length=300)
    telefono = models.CharField(max_length=15)
    telefono2 = models.CharField(max_length=150,blank=True,null=True)
    correo = models.EmailField(max_length=100, blank=True,null=True,default='contacto@teotihuacanmagico.com.mx')
    facebook = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
    twitter =models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
    instagram = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
    youtube = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
    caracteristicas = models.TextField(max_length=300)
    horario = models.CharField(max_length=300, blank=True,null=True)
    image = models.ImageField(upload_to='portadas', default='img/teo2.png', null=True, blank=True)
    imagedos = models.ImageField(upload_to='portadas', default='img/teo2.png', null=True, blank=True)
    imagetres = models.ImageField(upload_to='portadas', default='img/teo2.png', null=True, blank=True)
    lat = models.FloatField(null=True, blank=True, default=None)
    lng = models.FloatField(null=True, blank=True, default=None)
    available = models.BooleanField(default=True)

    def __str__(self):
        return self.nombre
    
    def get_absolute_url(self):
        return reverse('servicios:servicio_detail', args=[self.slug])

class Marcador(models.Model):
    servicio = models.ForeignKey(Servicio, related_name='servicio')
    nombre = models.CharField(max_length=250, blank=True, null=True)
    latitud = models.FloatField(null=True, blank=True, default=None)
    longitud= models.FloatField(null=True, blank=True, default=None)

    def __str__(self):
        return self.nombre
