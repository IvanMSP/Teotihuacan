from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from .models import Servicio,CategoriaServicio,Marcador

class Servicio_List(View):
	def get(self,request,categoria_slug = None):
		template_name = "servicio_list.html"
		categoria = None
		categorias = CategoriaServicio.objects.all()
		servicios = Servicio.objects.filter(available = True)
		if categoria_slug:
			categoria = get_object_or_404(CategoriaServicio,slug = categoria_slug)
			servicios = servicios.filter(categoria=categoria)
		context = {
			'categoria': categoria,
			'categorias': categorias,
			'servicios':servicios,
		}
		return render(request,template_name,context)

class Servicio_Detail(View):
	def get(self, request,slug):
		template_name = "servicio_detail.html"
		servicio = get_object_or_404(Servicio, slug = slug)
		marcadores = Marcador.objects.all().filter(servicio = servicio)
		context={
			'marcadores': marcadores,
			'servicio':servicio,
		}
		print(marcadores)
		return render(request,template_name,context)