from __future__ import unicode_literals
from django.db import models
from django.core.urlresolvers import reverse


class CategoriaRest(models.Model):
	nombre = models.CharField(max_length=250,db_index=True)
	slug = models.CharField(max_length=250,db_index=True,unique=True)

	class Meta():
		ordering = ('nombre',)
		verbose_name='categoria_restaurant'
		verbose_name_plural = 'categorias_restaurant'

	def __str__(self):
		return self.nombre

	def get_absolute_url(self):
		return reverse('restaurantes:restaurantes-list-by-categoria',args=[self.slug])

# Create your models here.
class Restaurant(models.Model):
	categoria = models.ForeignKey(CategoriaRest,related_name='categoria',blank=True,null=True)
	nombre = models.CharField(max_length=100)
	slug = models.SlugField(max_length=100)
	descripcion = models.TextField()
	logo = models.ImageField(upload_to='logo-restaurantes', default='img/teo2.png',blank=True, null=True)
	portada_uno = models.ImageField(upload_to="portadas-restaurantes", default='img/teo2.png',blank=True,null=True)
	portada_dos = models.ImageField(upload_to='portadas-restaurantes', default='img/teo2.png',blank=True,null=True)
	portada_tres = models.ImageField(upload_to='portadas-restaurantes', default='img/teo2.png',blank=True,null=True)
	ubicacion = models.CharField(max_length=300)
	lat = models.FloatField(null=True,blank=True,default=None)
	lng = models.FloatField(null=True,blank=True,default=None)
	telefono_uno = models.CharField(max_length=50)
	telefono_dos = models.CharField(max_length=50, blank=True, null=True)
	paginaWeb = models.CharField(max_length=300)
	correo = models.EmailField(max_length=100, blank=True,null=True,default='contacto@teotihuacanmagico.com.mx')
	facebook = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
	twitter =models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
	instagram = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
	youtube = models.URLField(max_length=200, blank=True,null=True,default='https://www.facebook.com/teotihuacanmagicomx/')
	horario = models.CharField(max_length=250)
	menu = models.ImageField(upload_to="menu-restaurant", default='img/teo2.png',blank=True,null=True)
	menu_image = models.ImageField(upload_to = "menu-restaurant" , default = 'img/teo2.png', blank = True, null = True)
	menu_image_uno = models.ImageField(upload_to = "menu-restaurant" , default = 'img/teo2.png', blank = True, null = True)

	def __str__(self):
		return self.nombre

	def get_absolute_url(self):
		return reverse('restaurantes:restaurant_detail',args=[self.slug])

class Marcador(models.Model):
	restaurant = models.ForeignKey(Restaurant, related_name='restaurant')
	nombre = models.CharField(max_length=250, blank=True,null=True)
	latitud = models.FloatField(null=True,blank=True,default=None)
	longitud = models.FloatField(null=True,blank=True,default=None)

	def __str__(self):
		return self.nombre