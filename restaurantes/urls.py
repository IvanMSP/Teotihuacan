from django.conf.urls import url
from . import views

urlpatterns =[
	url(r'^restaurant-list/$',views.Restaurant_List.as_view(), name="restaurant_list"),
	url(r'^restaurant-detail/(?P<slug>[-\w]+)/$',views.Restaurant_Detail.as_view(),name="restaurant_detail"),
	url(r'^restaurant-list/(?P<categoria_slug>[-\w]+)/$',views.Restaurant_List.as_view(),name='restaurantes-list-by-categoria'),
	# url(r'^restaurant-detail/(?P<id>\d+)/$',views.Restaurant_Detail.as_view(), name="restaurant_detail"),
]