from django.shortcuts import render, get_object_or_404
from django.views.generic import View
from .models import Restaurant,CategoriaRest,Marcador

class Restaurant_List(View):
	def get(self,request,categoria_slug=None):
		template_name = "restaurant_list.html"
		categoria_restaurant = None
		categorias_restaurant = CategoriaRest.objects.all()
		restaurantes = Restaurant.objects.all()
		if categoria_slug:
			categoria_restaurant = get_object_or_404(CategoriaRest,slug=categoria_slug)
			restaurantes = restaurantes.filter(categoria=categoria_restaurant)
		context = {
			'categoria_restaurant':categoria_restaurant,
			'categorias_restaurant':categorias_restaurant,
			'restaurantes':restaurantes,
		}
		return render(request,template_name,context)

class Restaurant_Detail(View):
	def get(self,request,slug):
		template_name = "restaurant_detail.html"
		restaurant = get_object_or_404(Restaurant,slug=slug)
		marcadores = Marcador.objects.all().filter(restaurant = restaurant)
		context = {
			'restaurant':restaurant,
			'marcadores':marcadores,
		}
		return render(request,template_name,context)