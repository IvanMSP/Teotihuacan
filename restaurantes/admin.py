from django.contrib import admin
from .models import Restaurant,CategoriaRest,Marcador
from pagedown.widgets import AdminPagedownWidget
from django.db import models
# Register your models here.
class RestaurantAdmin(admin.ModelAdmin):
	prepopulated_fields = {"slug":('nombre',)}
	formfield_overrides={
		models.TextField:{'widget':AdminPagedownWidget},
	}

class CategoriaRestAdmin(admin.ModelAdmin):
	prepopulated_fields = {'slug':('nombre',)}

admin.site.register(Restaurant,RestaurantAdmin)
admin.site.register(CategoriaRest,CategoriaRestAdmin)
admin.site.register(Marcador)