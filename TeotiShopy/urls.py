"""TeotiShopy URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.10/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url,include
from django.contrib import admin

from home import urls as UrlsHome
from actividades import urls as UrlsActividades
from eventos import urls as UrlsEventos
from alojamiento import urls as UrlsAlojamientos
from blog import urls as UrlsBlog
from restaurantes import urls as UrlsRestaurantes
from servicios import urls as UrlsServicios
from tour import urls as UrlsTour
from shop import urls as UrlsShop
from ruta import urls as UrlsRuta

from django.views.static import serve
from django.conf import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^blog/',include(UrlsBlog,namespace="blog")),
    url(r'^',include(UrlsHome,namespace="home")),
    url(r'^',include(UrlsEventos,namespace="eventos")),
    url(r'^',include(UrlsActividades,namespace="actividades")),
    url(r'^',include(UrlsAlojamientos,namespace="alojamientos")), 
    url(r'^',include(UrlsRestaurantes,namespace="restaurantes")),
    url(r'^',include(UrlsServicios,namespace="servicios")),
    url(r'^',include(UrlsTour,namespace="tours")),
    url(r'^',include(UrlsShop,namespace="shop")),
    url(r'^',include(UrlsRuta,namespace="ruta")),        
    url(
    	regex=r'^media/(?P<path>.*)$',
    	view=serve,
    	kwargs = {'document_root':settings.MEDIA_ROOT}),
]
